<?php
/*
 * Template Name: Sitemap Page

 * @link https://developer.wordpress.org/themes/template-files-section/page-template-files/
 *
 * @package WordPress
 * @subpackage code_and_create
 * @since CNCStarter 1.0.0
*/

get_header();

if ( have_posts() ) {

	// Load posts loop.
	while ( have_posts() ) {
		the_post();
        get_template_part('template-parts/content/content', 'sitemap'); 
	}

} else {
	// If no content, include the "No posts found" template.
	get_template_part( 'template-parts/content/content-none' );
}

get_footer();