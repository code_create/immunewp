<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage code_and_create
 * @since Code and Create 1.0
 */
define("ENQUEUE_SCRIPT_IN_FOOTER",  true );
if ( ! function_exists( 'code_and_create_setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
	 * @since Code and Create 1.0
	 *
	 * @return void
	 */
	function code_and_create_setup() {
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * This theme does not use a hard-coded <title> tag in the document head,
		 * WordPress will provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/**
		 * Add post-formats support.
		 */
		add_theme_support(
			'post-formats',
			array(
				'link',
				'aside',
				'gallery',
				'image',
				'quote',
				'status',
				'video',
				'audio',
				'chat',
			)
		);

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1568, 9999 );

		register_nav_menus(
			array(
				'primary' => esc_html__( 'Primary menu', 'codeandcreate' ),
				'footer'  => __( 'Secondary menu', 'codeandcreate' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
				'navigation-widgets',
			)
		);

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		$logo_width  = 300;
		$logo_height = 100;

		add_theme_support(
			'custom-logo',
			array(
				'height'               => $logo_height,
				'width'                => $logo_width,
				'flex-width'           => true,
				'flex-height'          => true,
				// 'unlink-homepage-logo' => true,
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add support for Block Styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );
		// Add support for editor styles.
		add_theme_support( 'editor-styles' );
		
		$editor_stylesheet_path = './assets/css/style-editor.css';
		add_editor_style( $editor_stylesheet_path );

		// Note, the is_IE global variable is defined by WordPress and is used
		// to detect if the current browser is internet explorer.
		global $is_IE;

		// Add custom editor font sizes.
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => esc_html__( 'Extra small', 'codeandcreate' ),
					'shortName' => esc_html_x( 'XS', 'Font size', 'codeandcreate' ),
					'size'      => 15,
					'slug'      => 'extra-small',
				),
				array(
					'name'      => esc_html__( 'Small', 'codeandcreate' ),
					'shortName' => esc_html_x( 'S', 'Font size', 'codeandcreate' ),
					'size'      => 17,
					'slug'      => 'small',
				),
				array(
					'name'      => esc_html__( 'Normal', 'codeandcreate' ),
					'shortName' => esc_html_x( 'M', 'Font size', 'codeandcreate' ),
					'size'      => 22,
					'slug'      => 'normal',
				),
				array(
					'name'      => esc_html__( 'Large', 'codeandcreate' ),
					'shortName' => esc_html_x( 'L', 'Font size', 'codeandcreate' ),
					'size'      => 30,
					'slug'      => 'large',
				),
				array(
					'name'      => esc_html__( 'Extra large', 'codeandcreate' ),
					'shortName' => esc_html_x( 'XL', 'Font size', 'codeandcreate' ),
					'size'      => 40,
					'slug'      => 'extra-large',
				),
				array(
					'name'      => esc_html__( 'Huge', 'codeandcreate' ),
					'shortName' => esc_html_x( 'XXL', 'Font size', 'codeandcreate' ),
					'size'      => 96,
					'slug'      => 'huge',
				),
				array(
					'name'      => esc_html__( 'Gigantic', 'codeandcreate' ),
					'shortName' => esc_html_x( 'XXXL', 'Font size', 'codeandcreate' ),
					'size'      => 144,
					'slug'      => 'gigantic',
				),
			)
		);

		// Custom background color.
		add_theme_support(
			'custom-background',
			array(
				'default-color' => 'FFFFFF',
			)
		);

		// Editor color palette.
		$black     = '#1D1D1D';
		$white     = '#FFFFFF';
		$yellow    = '#FCFA43';

		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => esc_html__( 'Black', 'codeandcreate' ),
					'slug'  => 'black',
					'color' => $black,
				),
				array(
					'name'  => esc_html__( 'Yellow', 'codeandcreate' ),
					'slug'  => 'yellow',
					'color' => $yellow,
				),
				array(
					'name'  => esc_html__( 'White', 'codeandcreate' ),
					'slug'  => 'white',
					'color' => $white,
				),
			)
		);

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );

		// Add support for experimental cover block spacing.
		add_theme_support( 'custom-spacing' );
	}
}
add_action( 'after_setup_theme', 'code_and_create_setup' );

/**
 * Register widget area.
 *
 * @since Code and Create 1.0
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 *
 * @return void
 */
function code_and_create_widgets_init() {

	register_sidebar(
		// array(
		// 	'name'          => esc_html__( 'Footer', 'codeandcreate' ),
		// 	'id'            => 'sidebar-1',
		// 	'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'codeandcreate' ),
		// 	'before_widget' => '<section id="%1$s" class="widget %2$s">',
		// 	'after_widget'  => '</section>',
		// 	'before_title'  => '<h2 class="widget-title">',
		// 	'after_title'   => '</h2>',
		// )
		array(
			'name'          => esc_html__( 'Header Widgets', 'codeandcreate' ),
			'id'            => 'cta-widgets',
			'description'   => esc_html__( 'Add widgets here to appear next to your primary navigation.', 'codeandcreate' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

  	register_sidebar(
		  array(
			'name' => 'cartholder',
			'id' => 'cartholder-widgets',
			'before_widget' => '<div class="cartarea">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>',
			)
		);
		
	register_sidebar(
		array(
			'name' => 'footer-widgets',
			'id' => 'footer-widgets',
			'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'codeandcreate' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s footer__widgets">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
			)
		);
}
add_action( 'widgets_init', 'code_and_create_widgets_init' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @since Code and Create 1.0
 *
 * @global int $content_width Content width.
 *
 * @return void
 */
function code_and_create_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'code_and_create_content_width', 750 );
}
add_action( 'after_setup_theme', 'code_and_create_content_width', 0 );

/**
 * Enqueue scripts and styles.
 *
 * @since Code and Create 1.0
 *
 * @return void
 */
function code_and_create_scripts() {
	// Note, the is_IE global variable is defined by WordPress and is used
	// to detect if the current browser is internet explorer.
	global $is_IE, $wp_scripts;	
	wp_enqueue_style( 
		'code-and-create-google-fonts', // Unique Style Name
		"https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;700&display=swap", // File Location
		array(), // Dependencies
		wp_get_theme()->get( 'Version' ) // Version
	);
	wp_enqueue_style( 
		'code-and-create-google-oswald', // Unique Style Name
		"https://fonts.googleapis.com/css2?family=Oswald:wght@400;600&display=swap", // File Location
		array(), // Dependencies
		wp_get_theme()->get( 'Version' ) // Version
	);
	wp_enqueue_style( 
		'code-and-create-style', // Unique Style Name
		get_template_directory_uri() . '/style.css', // File Location
		array(), // Dependencies
		wp_get_theme()->get( 'Version' ) // Version
	);

	// Main navigation scripts.
	if ( has_nav_menu( 'primary' ) ) {
		wp_enqueue_script( 
			'code-and-create-primary-navigation-script', // Unique Name
			get_template_directory_uri() . '/assets/js/primary-navigation.js', // file location
			array( 'jquery' ), // Dependencies
			wp_get_theme()->get( 'Version' ), // version
			ENQUEUE_SCRIPT_IN_FOOTER  
		);
	}
	wp_enqueue_style( 
		'code-and-create-glide', // Unique Style Name
		get_template_directory_uri() . '/assets/glide/css/glide.core.css', // File Location
		array(), // Dependencies
		wp_get_theme()->get( 'Version' ) // Version
	);
	wp_enqueue_style( 
		'code-and-create-glide-theme', // Unique Style Name
		get_template_directory_uri() . '/assets/glide/css/glide.theme.css', // File Location
		array("code-and-create-glide"), // Dependencies
		wp_get_theme()->get( 'Version' ) // Version
	);

	wp_enqueue_script( 
		'code-and-create-glide-script', // Unique Name
		get_template_directory_uri() . '/assets/glide/glide.min.js', // file location
		array(), // Dependencies
		wp_get_theme()->get( 'Version' ), // version
		ENQUEUE_SCRIPT_IN_FOOTER  
	);

	wp_enqueue_script( 
		'code-and-create-script', // Unique Name
		get_template_directory_uri() . '/assets/js/script.js', // file location
		array('code-and-create-glide-script'), // Dependencies
		wp_get_theme()->get( 'Version' ), // version
		ENQUEUE_SCRIPT_IN_FOOTER 
	);

}
add_action( 'wp_enqueue_scripts', 'code_and_create_scripts' ); 

/**
 * Enqueue Block Editor script
 *
 * @since Code and Create 1.0
 *
 * @return void
 */
function code_and_create_editor_scripts() {
	wp_enqueue_script(
		'code-and-create-editor-script', 
		get_stylesheet_directory_uri() . '/assets/js/editor.js', 
		array( 'wp-blocks', 'wp-dom' ), 
		wp_get_theme()->get( 'Version' ),
		true
	);
	
}

add_action( 'enqueue_block_editor_assets', 'code_and_create_editor_scripts' );

/**
 * Calculate classes for the main <html> element.
 *
 * @since 1.0.0
 *
 * @return void
 */
function codeandcreate_the_html_classes() {
	$classes = apply_filters( 'codeandcreate_html_classes', '' );
	if ( ! $classes ) {
		return;
	}
	echo 'class="' . esc_attr( $classes ) . '"';
}

function add_file_types_to_uploads($file_types){
	$new_filetypes = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types = array_merge($file_types, $new_filetypes );
	return $file_types;
	}
	add_filter('upload_mimes', 'add_file_types_to_uploads');

// SVG Icons class.
require get_template_directory() . '/classes/class-code-and-create-svg-icons.php';

// Customizer additions.
require get_template_directory() . '/classes/class-code-and-create-customize.php';
new Code_And_Create_Customize();

// Custom Button Widget.
require get_template_directory() . '/classes/class-code-and-create-button-widget.php';
new Code_And_Create_Button_Widget();

// Enhance the theme by hooking into WordPress.
require get_template_directory() . '/inc/template-functions.php';

require get_template_directory() . '/inc/template-tags.php';

require get_template_directory() . '/inc/shortcodes.php';

require get_template_directory() . '/inc/woocommerce-shortcodes.php';

require get_template_directory() . '/inc/template-types.php';

/**
 * Load WooCommerce compatibility file.
 */
 if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
	require get_template_directory() . '/inc/woocommerce-hooks.php';
}
