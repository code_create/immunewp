<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage code_and_create
 * @since Twenty Twenty-One 1.0
 */

?>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">

		<div class="footer__inner">
			<div class="footer__row">
				<div class="footer__col">
					<?php get_template_part( 'template-parts/footer/footer-mailchimp' ); ?>
				</div>	
				<div class="footer__col">
					<?php if ( is_active_sidebar( 'footer-widgets' ) ) : ?>
					<div class="site-cta">
						<?php dynamic_sidebar('footer-widgets'); ?>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="footer__row">
				<div class="footer__col">
					<?php if ( has_nav_menu( 'footer' ) ) : ?>
						<nav aria-label="<?php esc_attr_e( 'Secondary menu', 'codeandcreate' ); ?>" class="footer__navigation">
							<ul class="footer__navigation-wrapper">
								<?php
									wp_nav_menu(
										array(
											'theme_location' => 'footer',
											'items_wrap'     => '%3$s',
											'container'      => false,
											'depth'          => 1,
											'link_before'    => '<span>',
											'link_after'     => '</span>',
											'fallback_cb'    => false,
										)
									);
								?>
							</ul><!-- .footer__navigation-wrapper -->
						</nav><!-- .footer__navigation -->
					<?php endif; ?>
					<p class="footer__copyright">© Copyright 2021 Immune™ Pure Power. All Rights Reserved.</p>
					<small>*Disclaimer; the statements made on this website have not been evaluated by the Food & Drug Administration. These products are not intended to diagnose, prevent, treat, or cure any disease.</small>
				</div>
				<div class="footer__col">
					<div class="footer_payment-methods">
							<?php
								$asset_dir = get_template_directory_uri() . '/assets/images/';
							?>
							<img class="footer_payment-method" src="<?= $asset_dir ?>VISA.svg" alt="Pay with VISA">
							<img class="footer_payment-method" src="<?= $asset_dir ?>MasterCard.svg" alt="Pay with MasterCard">
							<img class="footer_payment-method" src="<?= $asset_dir ?>PayPal.svg" alt="Pay with PayPal">
							<img class="footer_payment-method" src="<?= $asset_dir ?>Amex.svg" alt="Pay with American Express">
						</div>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
