wp.domReady( () => {

	wp.blocks.registerBlockStyle( 'core/group', [ 
		{
			name: 'default',
			label: 'Default',
			isDefault: true,
		},
		{
			name: 'card-white',
			label: 'white Card',
		},
	]);

    wp.blocks.unregisterBlockStyle(
		'core/button',
		[ 'outline', 'fill' ]
	);

    wp.blocks.registerBlockStyle( 'core/button', [ 
		{
			name: 'default',
			label: 'Default',
			isDefault: true,
		},
		{
			name: 'yellow-button',
			label: 'Yellow Button',
		},
        {
			name: 'black-button',
			label: 'Black Button',
		}
	]);

	wp.blocks.registerBlockStyle( 'core/columns', [ 
		{
			name: 'default',
			label: 'Default',
			isDefault: true,
		},
        {
			name: 'no-padding',
			label: 'No padding',
		}
    ]);

    wp.blocks.registerBlockStyle( 'core/spacer', [ 
		{
			name: 'default',
			label: 'Default',
			isDefault: true,
		},
        {
			name: 'negative',
			label: 'Negative Space',
			isDefault: true,
		}
    ]);

    wp.blocks.registerBlockStyle( 'core/embed', [ 
		{
			name: 'default',
			label: 'Default',
			isDefault: true,
		},
        {
			name: 'rounded',
			label: 'Rounded',
			isDefault: true,
		}
    ]);

} );