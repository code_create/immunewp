(function ($) {
    $menu = $("#site-navigation-mobile");
    $menuButton = $("#primary-mobile-menu");
    $menuButton.on("click", function(){
        $menu.fadeToggle();
        $ariaExpanded = $menuButton.attr("aria-expanded") === "true" ? "false" : "true" ;
        $menuButton.attr("aria-expanded", $ariaExpanded );
        $('#primary-mobile-menu .dropdown-icon').toggle();
    })
})(jQuery);