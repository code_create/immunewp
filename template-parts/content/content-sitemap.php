<?php
/**
 * Template part for displaying a generic HTML sitemap
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage code_and_create
 * @since CNCStarter 1.0.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php if ( ! is_front_page() ) : ?>
		<header class="entry-header gutenberg-content">
			<?php get_template_part( 'template-parts/header/entry-header' ); ?>
		</header>
	<?php elseif ( has_post_thumbnail() ) : ?>
		<header class="entry-header gutenberg-content">
		</header>
	<?php endif; ?>

	<div class="entry-content gutenberg-content">
		<?php
		the_content();
        ?>
        <h2>Pages</h2>
        <ul>
        <?php
            wp_list_pages( array( 
            'exclude' => '',
            'title_li' => '',
            ) ); 
        ?>
        </ul>
    
        <h2>Posts</h2>
        <?php 
        $cats = get_categories('exclude=');
        foreach ($cats as $cat) :
            echo '<h3>' . $cat->cat_name . '</h3>';
            echo '<ul>';
            query_posts('posts_per_page=-1&cat=' . $cat->cat_ID);
            while( have_posts() ) {
                the_post();
                $category = get_the_category();
                if ($category[0]->cat_ID == $cat->cat_ID) {
                    echo sprintf(
                        '<li><a href="%s">%s</a></li>',
                        get_permalink(),
                        get_the_title()
                    ); 
                }
            }

            wp_reset_query();
            echo '</ul>';
        endforeach;
		?>
	</div><!-- .entry-content -->
</article> <!-- #post-<?php the_ID(); ?> -->