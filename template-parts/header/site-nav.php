<?php
/**
 * Displays the site navigation.
 *
 * @package WordPress
 * @subpackage code_and_create
 * @since Twenty Twenty-One 1.0
 */

?>

<?php if ( has_nav_menu( 'primary' ) ) : ?>
	<div class="menu-button-container">
		<button id="primary-mobile-menu" class="button" aria-controls="primary-menu-list" aria-expanded="false">
			<span class="dropdown-icon open"><?php esc_html_e( '', 'codeandcreate' ); ?>
				<?php echo code_and_create_get_icon_svg( 'ui', 'menu' ); // phpcs:ignore WordPress.Security.EscapeOutput ?>
			</span>
			<span class="dropdown-icon close"><span class="screen-reader-text"><?php esc_html_e( 'Close', 'codeandcreate' ); ?></span>
				<?php echo code_and_create_get_icon_svg( 'ui', 'close' ); // phpcs:ignore WordPress.Security.EscapeOutput ?>
			</span>
		</button><!-- #primary-mobile-menu -->
	</div><!-- .menu-button-container -->

	<nav id="site-navigation" class="primary-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary menu', 'codeandcreate' ); ?>">
		<?php
		wp_nav_menu(
			array(
				'theme_location'  => 'primary',
				'menu_class'      => 'menu-wrapper',
				'container_class' => 'primary-menu-container',
				'items_wrap'      => '<ul id="primary-menu-list" class="%2$s">%3$s</ul>',
				'fallback_cb'     => false,
			)
		);
		?>
	</nav><!-- #site-navigation -->

<?php endif; ?>

<?php if ( is_active_sidebar( 'cta-widgets' ) ) : ?>
	<div class="site-cta">
		<?php dynamic_sidebar('cta-widgets'); ?>
	</div>
<?php endif; ?>
<?php
	if( function_exists('cac_woocommerce_header_cart') ) {
		cac_woocommerce_header_cart();
	}
?>
