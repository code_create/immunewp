<?php
/**
 * Displays the site header.
 *
 * @package WordPress
 * @subpackage code_and_create
 * @since Twenty Twenty-One 1.0
 */

$wrapper_classes  = 'site-header';
$wrapper_classes .= has_custom_logo() ? ' has-logo' : '';
$wrapper_classes .= true === get_theme_mod( 'display_title_and_tagline', true ) ? ' has-title-and-tagline' : '';
$wrapper_classes .= has_nav_menu( 'primary' ) ? ' has-menu' : '';
?>

<header id="masthead" class="<?php echo esc_attr( $wrapper_classes ); ?>" role="banner">
	<div class="masthead__inner">
		<?php get_template_part( 'template-parts/header/site-branding' ); ?>
		<!-- <div><button class="nav-btn is-style-yellow-button">BUY NOW</button></div> -->
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("cartholder") ) : ?>
        <?php endif;?>
		<?php get_template_part( 'template-parts/header/site-nav' ); ?>
	</div>
</header><!-- #masthead -->
<nav id="site-navigation-mobile" class="primary-navigation primary-navigation-mobile" role="navigation" aria-label="<?php esc_attr_e( 'Primary menu', 'codeandcreate' ); ?>">
		<?php
		wp_nav_menu(
			array(
				'theme_location'  => 'primary',
				'menu_class'      => 'menu-wrapper',
				'container_class' => 'primary-menu-container',
				'items_wrap'      => '<ul id="primary-menu-list" class="%2$s">%3$s</ul>',
				'fallback_cb'     => false,
			)
		);
		?>
</nav><!-- #site-navigation -->
