<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage code_and_create
 * @since Twenty Twenty-One 1.0
 */

get_header();
$faq_taxonomy_slug = 'code_and_create_faq_category';
$faq_cat = get_terms( $faq_taxonomy_slug );
$description = get_the_archive_description();
?>
<header class="page-header alignwide">
    <h1 class="page-title">
	    <?php post_type_archive_title(); ?>
	</h1>
</header><!-- .page-header -->
<div class="entry-content">
<?php
    foreach ( $faq_cat as $cat ) {
        $cat_query = new WP_Query( array(
            'post_type' => 'code_and_create_faq',
            'tax_query' => array(
                array(
                    'taxonomy' => $faq_taxonomy_slug,
                    'field' => 'slug',
                    'terms' => array( $cat->slug ),
                    'operator' => 'IN'
                )
            )
        ) );
        ?>
        <div class="FAQs-shortcode alignwide">
            <div class="FAQs-shortcode__header">
                <h3 class="FAQs-shortcode__title"><?php echo $cat->name; ?></h3>
            </div>
            <div class="FAQs-shortcode__body">
                <?php if ( $cat_query->have_posts() ) : while ( $cat_query->have_posts() ) : $cat_query->the_post(); ?>
                    <details>
                        <summary><strong><?php the_title(); ?></strong></summary>
                        <p><?php the_content(); ?></p>
                    </details>
                <?php endwhile; endif; ?>
            </div>
        </div>
        <?php
        // Reset things, for good measure
        $cat_query = null;
        wp_reset_postdata();
    }
?>
</div>

<?php get_footer(); ?>
