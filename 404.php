<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage code_and_create
 * @since Twenty Twenty-One 1.0
 */

get_header();
?>

	<div class="error-404 not-found default-max-width gutenberg-content">
		<header class="page-header">
			<h1 class="page-title"><?php esc_html_e( 'Nothing here', 'immune' ); ?></h1>
			<p><?php esc_html_e( 'The page you were looking for was moved or removed. Maybe try a search?', 'immune' ); ?></p>
			<?php get_search_form(); ?>
		</header><!-- .page-header -->
		<div class="alignwide">
			<h2>Recently Viewed Products</h2>
			<?php echo do_shortcode('[recent_products limit=4]'); ?>
		</div>
		<div class="alignwide">
			<h2>Most Popular Products</h2>
			<?php echo do_shortcode('[best_selling_products limit=4]'); ?>
		</div>

	</div><!-- .error-404 -->


<?php
get_footer();
