<?php
/**
 * Custom icons for this theme.
 *
 * @package WordPress
 * @subpackage code_and_create
 * @since 1.0.0
 */

if ( ! class_exists( 'code_and_create_Button_Widget' ) ) {

    class Code_And_Create_Button_Widget extends WP_Widget {
        function __construct() {
            parent::__construct(
                'code_and_create_Button_Widget',  // Base ID
                __('Button', 'code_and_create'), // Name
                array( 'description' => __( 'Clickable button widget', 'code_and_create' ), ) // description 
            );

            add_action( 'widgets_init', function() {
                register_widget( 'code_and_create_Button_Widget' );
            });
        }
        
        // front-end
        public function widget( $args, $instance ) {
            $title = apply_filters( 'widget_title', $instance['title'] );
            $text = $instance['text'];
            $url = esc_url_raw($instance['url']);
            $styling = $instance['styling'];
  
            // before and after widget arguments are defined by themes
            echo $args['before_widget'];
            if ( ! empty( $title ) )
            echo $args['before_title'] . $title . $args['after_title'];

            // This is where you run the code and display the output
            echo '<div class="button-widget  wp-block-button is-style-' . $styling . '-button">';
                echo '<a class="wp-block-button__link button-widget__button button" href="' . $url . '">';
                echo esc_html__( $instance['text'], 'code_and_create' );
                echo '</a>';
            echo '</div>';
            echo $args['after_widget'];
        }
                
        // Backend 
        public function form( $instance ) {
            $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( '', 'code_and_create' );
            $text = ! empty( $instance['text'] ) ? $instance['text'] : esc_html__( '', 'code_and_create' );
            $url = ! empty( $instance['url'] ) ? $instance['url'] : esc_html__( '#', 'code_and_create' );
            $styling = ! empty( $instance['styling'] ) ? $instance['styling'] : esc_html__( 'green', 'code_and_create' );

            // Widget admin form
            ?>
                <p>
                    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
                    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
                </p>
                <p>
                    <label for="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>"><?php echo esc_html__( 'Text:', 'code_and_create' ); ?></label>
                    <textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'text' ) ); ?>" type="text" cols="30" rows="10"><?php echo esc_attr( $text ); ?></textarea>
                </p>
                <p>
                    <label for="<?php echo $this->get_field_id( 'url' ); ?>"><?php _e( 'URL:' ); ?></label> 
                    <input class="widefat" id="<?php echo $this->get_field_id( 'url' ); ?>" name="<?php echo $this->get_field_name( 'url' ); ?>" type="text" value="<?php echo esc_attr( $url ); ?>" />
                </p>
                <label for="<?php echo $this->get_field_id( 'styling' ); ?>"><?php _e( 'Style:' ); ?></label> 
                <select class="widefat" id="<?php echo $this->get_field_id( 'styling' ); ?>" name="<?php echo $this->get_field_name( 'styling' ); ?>">
                    <option value="yellow" <?php selected( $styling, 'yellow' ); ?>>Yellow</option>
                    <option value="black" <?php selected( $styling, 'black' ); ?>>Black</option>
                </select>
            <?php 
        }
            
        // Updating widget replacing old instances with new
        public function update( $new_instance, $old_instance ) {
            $instance = array();
            $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
            $instance['text'] = ( !empty( $new_instance['text'] ) ) ? $new_instance['text'] : '';
            $instance['url'] = ( !empty( $new_instance['url'] ) ) ? sanitize_url($new_instance['url']) : '';
            $instance['styling'] = ( !empty( $new_instance['styling'] ) ) ? $new_instance['styling'] : '';
     
            return $instance;
        }
    
    } 
}