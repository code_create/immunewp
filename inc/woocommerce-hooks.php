<?php 
/**
 * WooCommerce Hook files.
 * Remove, add and customize woocommerce template hooks
 *
 * @link https://woocommerce.com/
 *
 * @package code_and_create
 */

/**
 * Remove default WooCommerce wrapper.
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

// Unhook Breadcrumbs
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

/** 
 * Product Archive
 * Unhook result count and ordering product archive
 * Unhook Price & Rating on product archive
 */
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
add_action('woocommerce_before_shop_loop_item_title', 'immune_template_loop_product_brand', 20);

/** 
 * Single-product
 * Swap price & excerpt
 * unhook category
 */
remove_action('woocommerce_single_product_summary','woocommerce_template_single_price',10);
remove_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt',20);
remove_action('woocommerce_single_product_summary','woocommerce_template_single_meta',40);
add_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt',10);
add_action('woocommerce_single_product_summary','woocommerce_template_single_price', 20);

/** 
 * Mini-cart
 * Unhook cart buttons
 * Hook custom cart buttons
 */
remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_button_view_cart', 10 );
remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout', 20 );
add_action( 'woocommerce_widget_shopping_cart_buttons', 'immune_widget_shopping_cart_button_view_cart', 10 );
add_action( 'woocommerce_widget_shopping_cart_buttons', 'immune_widget_shopping_cart_proceed_to_checkout', 20 );

add_action('woocommerce_widget_shopping_cart_buttons', 'immune_widget_shopping_cart_button_view_cart', 10);
add_action('woocommerce_widget_shopping_cart_buttons', 'immune_widget_shopping_cart_proceed_to_checkout', 20);
