<?php
/**
 * WooCommerce Compatibility File
 *
 * @link https://woocommerce.com/
 *
 * @package cac
 */

/**
 * WooCommerce setup function.
 *
 * @link https://docs.woocommerce.com/document/third-party-custom-theme-compatibility/
 * @link https://github.com/woocommerce/woocommerce/wiki/Enabling-product-gallery-features-(zoom,-swipe,-lightbox)-in-3.0.0
 *
 * @return void
 */
 function cac_woocommerce_setup() {
	add_theme_support(
		'woocommerce',
		array(
			'thumbnail_image_width' => 150,
			'single_image_width'    => 300,
			'product_grid'          => array(
				'default_rows'    => 3,
				'min_rows'        => 1,
				'default_columns' => 4,
				'min_columns'     => 1,
				'max_columns'     => 6,
			),
		)
	);
}
add_action( 'after_setup_theme', 'cac_woocommerce_setup' );

/**
 * Add 'woocommerce-active' class to the body tag.
 *
 * @param  array $classes CSS classes applied to the body tag.
 * @return array $classes modified to include 'woocommerce-active' class.
 */
 function _s_woocommerce_active_body_class( $classes ) {
	$classes[] = 'woocommerce-active';

	return $classes;
}
add_filter( 'body_class', '_s_woocommerce_active_body_class' );

/**
 * Related Products Args.
 *
 * @param array $args related products args.
 * @return array $args related products args.
 */
function _s_woocommerce_related_products_args( $args ) {
	$defaults = array(
		'posts_per_page' => 3,
		'columns'        => 3,
	);

	$args = wp_parse_args( $defaults, $args );

	return $args;
}
add_filter( 'woocommerce_output_related_products_args', '_s_woocommerce_related_products_args' );

/**
 * Remove items from my account navigation
 *
 * @param array $items Navigation items
 * @return array $items Navigation items
 */
function immune_modify_myaccount_navigation_items($items) {
    unset($items['downloads']); // Remove downloads item
    unset($items['dashboard']); // Remove dashboard item
    unset($items['customer-logout']); // Remove logout item
    $items['edit-account'] = __('My Details', 'immune'); // Changing label for account details

    return $items;
}
add_filter( 'woocommerce_account_menu_items', 'immune_modify_myaccount_navigation_items' , 99, 1 );


if( ! function_exists ('immune_widget_shopping_cart_button_view_cart') ) {
	function immune_widget_shopping_cart_button_view_cart() {
		echo sprintf(
			'<div class="wp-block-button is-style-yellow-button"><a href="%s" class="button wp-block-button__link wc-forward">%s</a></div>',
			esc_url( wc_get_cart_url() ),
			esc_html__( 'View cart', 'woocommerce' )
		);
	}

}
if( ! function_exists ('immune_widget_shopping_cart_proceed_to_checkout') ) {
	function immune_widget_shopping_cart_proceed_to_checkout() {
		echo sprintf(
			'<div class="wp-block-button is-style-yellow-button"><a href="%s" class="button wp-block-button__link wc-forward wc-checkout">%s</a></div>', 
			esc_url( wc_get_checkout_url() ),
			esc_html__( 'Checkout', 'woocommerce' )
		);
	}
}

if ( ! function_exists ('immune_template_loop_product_brand') ) {
	/**
	 * Before Title.
	 *
	 * Add brand name before the product title on the product archive.
	 *
	 * @return void
	 */
	function immune_template_loop_product_brand(){
		?>
		<p class="woocommerce-loop-product__brand">Immune&trade; PurePower</p>
		<?php
	}
}

if ( ! function_exists( '_s_woocommerce_wrapper_before' ) ) {
	/**
	 * Before Content.
	 *
	 * Wraps all WooCommerce content in wrappers which match the theme markup.
	 *
	 * @return void
	 */
	function immune_woocommerce_wrapper_before() {
		?>
			<div id="woo-primary" class="woo-content">
		<?php
	}
}
add_action( 'woocommerce_before_main_content', 'immune_woocommerce_wrapper_before' );

if ( ! function_exists( '_s_woocommerce_wrapper_after' ) ) {
	/**
	 * After Content.
	 *
	 * Closes the wrapping divs.
	 *
	 * @return void
	 */
	function _s_woocommerce_wrapper_after() {
		?>
			</div><!-- #main -->
		<?php
	}
}
add_action( 'woocommerce_after_main_content', '_s_woocommerce_wrapper_after' );

if ( ! function_exists( 'immune_become_an_affiliate' ) ) {
	/**
	 * Before Content.
	 *
	 * Wraps all WooCommerce content in wrappers which match the theme markup.
	 *
	 * @return void
	 */
	function immune_become_an_affiliate() {
		echo do_shortcode('[affiliate_card]');
	}
}
// add_action( 'woocommerce_after_main_content', 'immune_become_an_affiliate', 15);

function cac_woocommerce_scripts() {

	$font_path   = WC()->plugin_url() . '/assets/fonts/';
	$inline_font = '@font-face {
		font-family: "star";
		src: url("' . $font_path . 'star.eot");
		src: url("' . $font_path . 'star.eot?#iefix") format("embedded-opentype"),
			url("' . $font_path . 'star.woff") format("woff"),
			url("' . $font_path . 'star.ttf") format("truetype"),
			url("' . $font_path . 'star.svg#star") format("svg");
		font-weight: normal;
		font-style: normal;
	}';
	$inline_woo_font = '@font-face {
		font-family:WooCommerce;
		src:url("' . $font_path . 'WooCommerce.eot");
		src:url("' . $font_path . 'WooCommerce.eot?#iefix") format("embedded-opentype"),
		url("' . $font_path . 'WooCommerce.woff") format("woff"),
		url("' . $font_path . 'WooCommerce.ttf") format("truetype"),
		url("' . $font_path . 'WooCommerce.svg#WooCommerce") format("svg");
		font-weight:400;
		font-style:normal
	}';
	wp_enqueue_style(
		'cac-woocommerce-style',
		get_template_directory_uri() . '/woocommerce.css',
		array(),
		wp_get_theme()->get( 'Version' )
	);
	wp_add_inline_style( 'cac-woocommerce-style', $inline_font );
	wp_add_inline_style( 'cac-woocommerce-style', $inline_woo_font );
}
add_action( 'wp_enqueue_scripts', 'cac_woocommerce_scripts' );


/**
 * Disable the default WooCommerce stylesheet.
 *
 * Removing the default WooCommerce stylesheet and enqueing your own will
 * protect you during WooCommerce core updates.
 *
 * @link https://docs.woocommerce.com/document/disable-the-default-stylesheet/
 */
 add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

 if ( ! function_exists( 'cac_woocommerce_cart_link_fragment' ) ) {
	/**
	 * Cart Fragments.
	 *
	 * Ensure cart contents update when products are added to the cart via AJAX.
	 *
	 * @param array $fragments Fragments to refresh via AJAX.
	 * @return array Fragments to refresh via AJAX.
	 */
	function cac_woocommerce_cart_link_fragment( $fragments ) {
		ob_start();
		cac_woocommerce_cart_link();
		$fragments['a.cart-contents'] = ob_get_clean();

		return $fragments;
	}
}
add_filter( 'woocommerce_add_to_cart_fragments', 'cac_woocommerce_cart_link_fragment' );

if ( ! function_exists( 'cac_woocommerce_cart_link' ) ) {
	/**
	 * Cart Link.
	 *
	 * Displayed a link to the cart including the number of items present and the cart total.
	 *
	 * @return void
	 */
	function cac_woocommerce_cart_link() {
		?>
		<a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'immune' ); ?>">
			<?php
			$item_count = WC()->cart->get_cart_contents_count();
			$item_count_text = sprintf(
				/* translators: number of items in the mini cart. */
				_n( '%d item', '%d items', $item_count, 'immune' ),
				$item_count
			);
			?>
			<img src="<?= get_template_directory_uri() ?>/assets/images/cart.png">
			<span class="count"><?php echo esc_html( $item_count_text ); ?></span>
			<?php
			if ($item_count > 0){
				echo'<span class="count-mobile">' . esc_html( $item_count ) . '</span>';
			}
			?> 
		</a>
		<?php
	}
}

if ( ! function_exists( 'cac_woocommerce_header_cart' ) ) {
	/**
	 * Display Header Cart.
	 *
	 * @return void
	 */
	function cac_woocommerce_header_cart() {
		if ( is_cart() ) {
			$class = 'current-menu-item';
		} else {
			$class = '';
		}
		?>
		<ul id="site-header-cart" class="site-header-cart">
			<li class="<?php echo esc_attr( $class ); ?>">
				<?php cac_woocommerce_cart_link(); ?>
			</li>
			<li>
				<?php
				$instance = array(
					'title' => '',
				);

				the_widget( 'WC_Widget_Cart', $instance );
				?>
			</li>
		</ul>
		<?php
	}
}

function immune_woocommerce_category_links(){
	global $wp;
	// Get the current url and add a slash if it doesnt already have one.
	$this_url = trailingslashit( home_url( $wp->request ) );
	// Get all categories
	$product_categories = get_terms(
		array(
			'taxonomy'   => "product_cat",
		)
	);
	// If an error occured or no categories are found, exit early
	if( is_wp_error( $product_categories ) || count( $product_categories ) === 0 ){
		return;
	}
	echo 'Filter By';
	foreach ( $product_categories as $key => $category ) {
		$category_url = trailingslashit( get_term_link( $category ) );
		$link_classes = 'woo_category_links';
		if ( $category_url === $this_url ) {
			$link_classes .= ' current';
		}
		echo sprintf('<a class="%s" href="%s">%s</a>', $link_classes, $category_url, $category->name);
	}
}

add_action( 'wp_footer', 'immune_cart_refresh_update_qty' ); 
function immune_cart_refresh_update_qty() {
   if (is_cart()) {
      ?>
		<script type="text/javascript">
			var timeout;

			jQuery('div.woocommerce').on('change keyup mouseup', 'input.qty', function(){ // keyup and mouseup for Firefox support
				if (timeout != undefined) clearTimeout(timeout); //cancel previously scheduled event
				if (jQuery(this).val() == '') return; //qty empty, instead of removing item from cart, do nothing
				timeout = setTimeout(function() {
					jQuery('[name="update_cart"]').trigger('click');
				}, 1000 );
			});
		</script>
      <?php
   }
}

function immune_mini_cart() {

if ( ! WC()->cart->is_empty() ) : ?>

	<ul id="immune_mini_cart" class="woocommerce-mini-cart cart_list product_list_widget">
		<header class="immune_mini_cart--header">
			<p class="immune_mini_cart--title">Your order summary</p>
			<a href="<?= wc_get_cart_url() ?>">Edit</a>
		</header>
		<?php
		do_action( 'woocommerce_before_mini_cart_contents' );

		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				$product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key );
				$thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
				$product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
				$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
				?>
				<li class="woocommerce-mini-cart-item <?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key ) ); ?>">
					<?php if ( empty( $product_permalink ) ) : ?>
						<figure>
							<?php echo $thumbnail; ?>
						</figure>
						<div>
							<?php echo esc_html($product_name); ?>
							<?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s', $cart_item['quantity'], $product_price ) . '</span>', $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
						</div>
					<?php else : ?>
						<a href="<?php echo esc_url( $product_permalink ); ?>">
						<figure>
							<?php echo $thumbnail; ?>
						</figure>
						<div>
							<p class="woocommerce_widget_cart_item_title"><?php echo esc_html($product_name); ?></p>
							<?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s', $cart_item['quantity'], $product_price ) . '</span>', $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
						</div>
						</a>
					<?php endif; ?>
					<?php echo wc_get_formatted_cart_item_data( $cart_item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
				</li>
				<?php
			}
		}

		do_action( 'woocommerce_mini_cart_contents' );
		?>
	</ul>

<?php 
endif;

}

add_filter ( 'wc_add_to_cart_message_html', 'wc_add_to_cart_message_filter', 10, 2 );
function wc_add_to_cart_message_filter($message, $products = null) {

    $ids = array_keys($products);
    $titles = array_map( 
        function( $product ) {
            $wc_product = wc_get_product( $product );
            return $wc_product->get_title();
        },
        $ids
    );

    $added_text = sprintf(
         _n( '%s has been added to your cart.', '%s have been added to your cart.', sizeof( $titles ), 'woocommerce' ),
        wc_format_list_of_items( $titles )
    );

    $view_cart_btn = sprintf(
        '<div class="wp-block-button is-style-yellow-button"><a href="%s" class="wp-block-button__link button">%s</a></div>',
        esc_url( wc_get_page_permalink( 'cart' ) ),
        esc_html__( 'View Cart', 'woocommerce' )
    );
    $checkout_btn = sprintf(
        '<div class="wp-block-button is-style-yellow-button"><a href="%s" class="wp-block-button__link button">%s</a></div>',
        esc_url( wc_get_page_permalink( 'checkout' ) ),
        esc_html__( 'Checkout', 'woocommerce' ),
    );

    $allowable_html = wp_kses_allowed_html( 'post' );
    $message = sprintf( '%s &nbsp;<div class="wp-block-buttons">%s%s</div>',
        esc_html( $added_text ),
        wp_kses( $view_cart_btn, $allowable_html),
        wp_kses( $checkout_btn, $allowable_html)
    );
                    

    return $message;
}