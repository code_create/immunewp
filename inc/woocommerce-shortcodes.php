<?php
/**
 * WooCommerce Shortcodes File
 *
 * @link https://woocommerce.com/
 *
 * @package code_and_create
 */

if ( ! function_exists( 'immune_featured_products' ) ) :
	function code_and_create_featured_products($args){

		$atts = array_change_key_case( (array) $atts, CASE_LOWER );
		// override default attributes with user attributes
		$featured_product_params = shortcode_atts(
			array(
				'products_per_page' => 3,
				'limit' => '12',
				'prev_text' => 'previous',
				'next_text' => 'next',
				'align' => 'wide',
			), $atts
		);

		$safe_alignments = ['wide','full','default'];
		$wrapper_alignment = $featured_product_params['align'];
		if( ! in_array ( $wrapper_alignment, $safe_alignments ) ){
			$wrapper_alignment = 'default';
		}
		
		$query_params = array(
			'limit' => $featured_product_paramsfeatured_product_params['limit'],
			'post_status' => 'published',
			'visibility' => 'visible',
			'featured' => true,
		);
        
		$products = wc_get_products($query_params);
		if ( empty( $products ) ){
			return '';
		}
		ob_start();
		?>
		<div class="align<?= esc_html__( $featured_product_params['align'], 'immune' ) ?>">
		<?php
		if( $featured_product_params['products_per_page'] < count($products) ) : ?>
			<div role="button" class="featuredproduct__arrow featuredproduct__arrowleft">
				<span class="screen-reader-text">previous</span>
			</div>
			<div role="button" class="featuredproduct__arrow featuredproduct__arrowright">
				<span class="screen-reader-text">next</span>
			</div>
		<?php 
		endif;
		foreach ( $products as $product ):
		?>
			<div>
				<div class="featuredproduct">
					<img loading="lazy" width="173" class="featuredproduct__img" src="<?php echo esc_attr( wp_get_attachment_url( $product->get_image_id() ) ); ?>" alt=""/>
					<?php echo $product->get_categories( ', ', '<span class="featuredproduct__category featuredproduct__meta">', '</span>' ); ?>
					<p class="featuredproduct__title featuredproduct__meta"><?= $product->name; ?></p>
					<p class="featuredproduct__description featuredproduct__meta"><?= $product->short_description; ?></p>
					<a class="button featuredproduct__link featuredproduct__meta" href="<?= esc_url(get_permalink( $product->id )); ?>">View Product</a>
				</div>
			</div>
		<?php
		endforeach;
		echo '</div>';
		$html = ob_get_clean();
		return sprintf('<div class="featured-products-shortcode alignfull"><div class="%s">%s</div></div>', 'align' . $wrapper_alignment, $html);
	}
	add_shortcode('immune-products-featured', 'immune_featured_products');
endif;