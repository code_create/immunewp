<?php

// Our custom post type function
function immune__reviews() {
    
    register_post_type( 'immune_reviews',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Reviews' ),
                'singular_name' => __( 'Review' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'review'),
            'show_in_rest' => true,
            'menu_icon'           => 'dashicons-star-half',
            'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'custom-fields'),
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'immune__reviews' );

/**
 * Register custom post types.
 *
 * @since 1.0.0
 *
 * @return void
 */
function code_and_create_custom_post_types() {
    register_post_type('code_and_create_faq',
        array(
            'labels'      => array(
                'name'          => __('FAQs', 'code_and_create'),
                'singular_name' => __('FAQ', 'code_and_create'),
            ),
			'public'      => true,
			'has_archive' => true,
			'rewrite'     => array( 'slug' => 'faqs' ),
        )
    );

	$args = array(
		'labels' => 
			array(
				'name'              => _x( 'FAQ Categories', 'code_and_create' ),
				'singular_name'     => _x( 'FAQ Category', 'code_and_create' ),
				'menu_name'         => __( 'FAQ Categories' ),
			),
		'hierarchical' => true,
	);
	register_taxonomy( 'code_and_create_faq_category', 'code_and_create_faq', $args );
}
add_action('init', 'code_and_create_custom_post_types');