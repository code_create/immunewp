
<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package WordPress
 * @subpackage code_and_create
 */

 /**
 * Pass in a taxonomy value that is supported by WP's `get_taxonomy`
 * and you will get back the url to the archive view.
 * @param $taxonomy string|int
 * @return string
 */
function code_and_create_taxonomy_archive_link( $taxonomy ) {
	$tax = get_taxonomy( $taxonomy ) ;
	return get_bloginfo( 'url' ) . '/' . $tax->rewrite['slug'];
}

/**
 * Adds custom classes to the array of body classes.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @param array $classes Classes for the body element.
 *
 * @return array
 */
function code_and_create_body_classes( $classes ) {

	// Helps detect if JS is enabled or not.
	$classes[] = 'no-js';

	// Adds `singular` to singular pages, and `hfeed` to all other pages.
	$classes[] = is_singular() ? 'singular' : 'hfeed';

	// Add a body class if main navigation is active.
	if ( has_nav_menu( 'primary' ) ) {
		$classes[] = 'has-main-navigation';
	}

	// Add a body class if there are no footer widgets.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-widgets';
	}

	return $classes;
}
add_filter( 'body_class', 'code_and_create_body_classes' );

/**
 * Gets the SVG code for a given icon.
 *
 * @param string $group The icon group.
 * @param string $icon The icon.
 * @param int    $size The icon size in pixels.
 *
 * @return string
 */
function code_and_create_get_icon_svg( $group, $icon, $size = 24 ) {
	return code_and_create_SVG_Icons::get_svg( $group, $icon, $size );
}