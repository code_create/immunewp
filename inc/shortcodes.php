<?php

function render_alert_shortcode($atts){

    $atts = array_change_key_case( (array) $atts, CASE_LOWER );
 
    // override default attributes with user attributes
    $alert_atts = shortcode_atts(
        array(
            'text' => 'Website alert',
            'url' => '',
            'urltext' => '',
        ), $atts
    );

    ob_start();
    ?>
    <div class="alert-shortcode is-style-card-blue">
        <span><?= esc_html__( $alert_atts['text'], 'code_and_create' )  ?></span>
        <?php
        if ( ! empty( $alert_atts['url'] ) && ! empty( $alert_atts['urltext'] ) ){
            echo sprintf(
                '<a href="%s"><strong>%s</strong></a>',
                esc_url( $alert_atts['url'] ),
                esc_html__( $alert_atts['urltext'], 'code_and_create' )
            );
        }
        ?>
        <button class="alert-shortcode__close" onclick="jQuery(this.parentElement).fadeOut()">
            <span class="screen-reader-text">Close this alert</span>
            <svg class="alert-shortcode__closeIcon" width="18" height="18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M18 15.429L15.429 18 9 11.571 2.571 18 0 15.429 6.429 9 0 2.571 2.571 0 9 6.429 15.429 0 18 2.571 11.571 9 18 15.429z" fill="#fff"/></svg>
        </button>
    </div>
    <?php
    return ob_get_clean();
}
add_shortcode('alert', 'render_alert_shortcode');

function code_and_create_latest_posts($atts){
    
    $atts = array_change_key_case( (array) $atts, CASE_LOWER );
 
    // override default attributes with user attributes
    $case_studies_atts = shortcode_atts(
        array(
            'headinglevel' => 'h2',
            'title' => 'Our Latest Posts',
            'align' => 'wide',
            'post_type' => 'post',
            'posts_per_page' => 3,
            'archive_link_text' => 'View all posts'
        ), $atts
    );
    
    $args = array(  
        'post_type' => $case_studies_atts['post_type'],
        'post_status' => 'publish',
        'posts_per_page' => $case_studies_atts['posts_per_page'],
    );

    $loop = new WP_Query( $args ); 
    if( ! $loop->have_posts() ){
        return '';
    }
    
    $archive_link = get_post_type_archive_link( $case_studies_atts['post_type'] );
        
    ob_start();
    ?>
    <section class="latest-posts alignfull">
        <div class="align<?= esc_html__( $case_studies_atts['align'], 'lignum' ) ?>">
            <div class="latest-posts--header">
                <span class="d-block text-center m-auto">
                    <h2 class="d-inlineblock"><?= esc_html__( $case_studies_atts['title'], 'lignum' ) ?></h2>
                    <a class="d-inlineblock" href="<?= $archive_link ?>">
                        <?= esc_html__( $case_studies_atts['archive_link_text'], 'lignum' ) ?>
                    </a>
                </span>
            </div>
            <div class="latest-posts--collection d-flex flex-wrap justify-center">
                <?php
                    while ( $loop->have_posts() ) : $loop->the_post(); 
                    get_template_part( 'template-parts/archive/content', get_post_type() );
                    endwhile;
                ?>
            </div>
        </div>
    </section>
    <?php
    wp_reset_postdata(); 
    return ob_get_clean();
}

add_shortcode('latest_posts', 'code_and_create_latest_posts'); 

function immune_home_header($atts){
    
    $atts = array_change_key_case( (array) $atts, CASE_LOWER );
 
    // override default attributes with user attributes
    $case_studies_atts = shortcode_atts(
        array(
            'headinglevel' => 'h2',
            'title' => 'Our Latest Posts',
            'align' => 'full',
        ), $atts
    );
    
    ob_start();
    ?>
    <section class="container" style="background-image:linear-gradient(#FCFA43, #FFD600); max-width:100%;margin: 0 0;width: 100%; overflow:hidden;">
        <div class="header-container" style="display:flex;">
            <div class="headers" style="margin: 150px 280px 150px 380px; color:black">
                <h2>JUST GET</h2>
                <h1>IMMUNE.</h1>
                <h3>NO CAFFEINE / MINIMAL SUGER / JUST VITAMINS</h3>
            </div>
            <div class="images-header">
                <div class="can-img" >
                    <img src="<?= get_template_directory_uri() ?> /assets/images/can.png"> 
                    <img src="<?= get_template_directory_uri() ?> /assets/images/can.png"> 
                    <img src="<?= get_template_directory_uri() ?> /assets/images/can.png"> 
                    <img src="<?= get_template_directory_uri() ?> /assets/images/can.png">
                </div>
            </div>
        </div>
    </section>
    <?php
    wp_reset_postdata(); 
    return ob_get_clean();
}

add_shortcode('home_header', 'immune_home_header'); 

function immune_review_slider($atts){
    
    $atts = array_change_key_case( (array) $atts, CASE_LOWER );
    // override default attributes with user attributes
    $immune_review_atts = shortcode_atts(
        array(
            'headinglevel' => 'h2',
            'title' => '<b>Immune™</b> Reviews',
            'align' => 'wide',
            'post_type' => 'immune_reviews',
            'posts_per_page' => 3,
            'archive_link_text' => 'View all posts'
        ), $atts
    );
    
    $args = array(  
        'post_type' => $immune_review_atts['post_type'],
        'post_status' => 'publish',
        'posts_per_page' => $immune_review_atts['posts_per_page'],
    );

    $loop = new WP_Query( $args ); 
    if( ! $loop->have_posts() ){
        return '';
    }
    
    $archive_link = get_post_type_archive_link( $immune_review_atts['post_type'] );

    ob_start();
    ?>
    <section 
        class="container immune-reviews" 
    >
        <div class="immune-reviews-wrapper">
            <div class="review-header">
            <?php
                echo sprintf(
                    '<%s class="immune-reviews-title">%s</%s>',
                    esc_html( $immune_review_atts['headinglevel'] ),
                    wp_kses( $immune_review_atts['title'], wp_kses_allowed_html( 'post' ) ),
                    esc_html( $immune_review_atts['headinglevel'] )
                );
            ?>
            </div>
            <div class="glide immune-review-glide">
                <div class="glide__track" data-glide-el="track">
                    <ul class="glide__slides">
                    <?php
                        while ( $loop->have_posts() ) : $loop->the_post(); 
                        ?>
                            <li class="glide-slide">
                                <article class="immune-review">
                                    <?= the_content(); ?>
                                </article>
                            </li>

                        <?php
                        endwhile;
                    ?>
                    </ul>
                </div>
                <div class="glide__arrows immune-reviews-arrows" data-glide-el="controls">
                    <button class="glide__arrow glide__arrow--left" data-glide-dir="<">
                        <span class="screen-reader-text">Previous Review</span>
                    </button>
                    <button class="glide__arrow glide__arrow--right" data-glide-dir=">">
                        <span class="screen-reader-text">Next Review</span>
                    </button>
                </div>
            </div>
        </div>
    </section>
    <?php
    wp_reset_postdata(); 
    return ob_get_clean();
}

add_shortcode('slider-review', 'immune_review_slider'); 



function immune_kaizen_rituals($atts){
    
    $atts = array_change_key_case( (array) $atts, CASE_LOWER );
 
    // override default attributes with user attributes
    $rituals_atts = shortcode_atts(
        array(
            'headinglevel' => 'h2',
            'title' => 'Our Latest Posts',
            'align' => 'wide',
            'post_type' => 'post',
            'posts_per_page' => 3,
            'archive_link_text' => 'View all posts'
        ), $atts
    );
    

    ob_start();
    ?>
    <section class="container kaizen-container" style="width:60%">
        <div class="is-style-card-white" style="padding: 25px; border-radius:8px;">
            <div class="kaizen-rituals">
                <div class="kaizen-rituals-main">
                    <h3 style="text-transform:uppercase;">Daily kaizen <strong>Immune&trade; rituals</strong></h3>
                    <p>For Full ‘Daily Kaizen IMMUNE&trade; Rituals’ Free PDF, enter details below.</p>
                    <input class="checkbox" type="checkbox" value="" name="Name"  style="border:none;">
                    <label for="checkbox">Tick to receive future emails. Read our <strong><a href="<?= esc_url(get_privacy_policy_url()) ?>">privacy policy</a></strong> for more info</label>
                </div>
                <div id="mc_embed_signup-kaizen">
                    <form action="" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">
                            <div class="mc-field-group">
                                <label for="mce-NAME">Name <strong>(required)</strong></label>
                                <input class="name-box" type="text" value="" name="Name" class="required" id="mce-NAME">
                            </div> 
                            <div class="mc-field-group">
                                <label for="mce-EMAIL">Email Address <strong>(required)</strong></span></label>
                                <input class="email-box" type="email" value="" name="Email" class="required" id="mce-EMAIL">
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div> <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_a9b785425f6d416aca65c2f84_aefea491d9" tabindex="-1" value=""></div>
                            <div class="clear wp-block-button is-style-black-button kaizen-submit" style="width:100%">
                                <button class="kaizen-button wp-block-button__link" style="text-transform: uppercase">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <?php
    wp_reset_postdata(); 
    return ob_get_clean();
}
add_shortcode('kaizen-rituals', 'immune_kaizen_rituals'); 


function immune_affiliate_card($atts) {
    $atts = array_change_key_case( (array) $atts, CASE_LOWER );
    // override default attributes with user attributes
    $affiliate_atts = shortcode_atts(
        array(
            'title' => 'Become an affiliate',
            'align' => 'wide',
            'text' => '',
            'button_text' => 'Get started',
            'button_url' => '#'
        ), $atts
    );
    
    ob_start();
    ?>
    <section class="affiliate-sc is-style-card-white">
        <div class="affiliate-sc-inner">
            <div class="affiliate-sc-img">
                <img src="<?= get_template_directory_uri(); ?>/assets/images/affiliate-can.png" alt="" />
            </div>
            <div class="affiliate-sc-text">
                <h2 class="affiliate-sc-title"><?= esc_html($affiliate_atts['title']); ?></h2>
                <p><?= esc_html($affiliate_atts['text']); ?></p>
                <div class="wp-block-button is-style-black-button">
                    <a class="wp-block-button__link button" href="<?= esc_html($affiliate_atts['button_url']); ?>">
                        <?= esc_html($affiliate_atts['button_text']); ?>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <?php
    return ob_get_clean();
}
add_shortcode('affiliate_card', 'immune_affiliate_card'); 

